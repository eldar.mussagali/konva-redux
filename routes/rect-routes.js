const { Router } = require('express')
const router = new Router()
const rects = require('../services/rects')

router.post(
    '/add',
    [],
    async (req, res) => {
        try {
            await rects.addRect(req.body)
            const coordinates = await rects.getAll()
            res.json(coordinates)
        } catch (err) {
            console.log(err)
        }
    }
)

router.post(
    '/delete',
    [],
    async (req, res) => {
        try {
            console.log(req.body);
            await rects.removeRect(req.body)
            const coordinates = await rects.getAll()
            res.json(coordinates)
        } catch (err) {
            console.log("Error in rect-routes/delete")
        }
    }
)

router.post(
    '/edit',
    [],
    async (req, res) => {
        try {
            await rects.editRect(req.body)
            const coordinates = await rects.getAll()
            res.json(coordinates)
        } catch (err) {
            console.log(err)
        }
    }
)

router.get(
    '/',
    async (req, res) => {
        const coordinates = await rects.getAll()
        res.json(coordinates)
    }
)

module.exports = router