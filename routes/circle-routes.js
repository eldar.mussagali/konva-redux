const { Router } = require('express')
const router = new Router()
const circles = require('../services/circles')

router.post(
    '/add',
    [],
    async (req, res) => {
        try {
            await circles.addCircle(req.body)
            const coordinates = await circles.getAll()
            res.json(coordinates)
        } catch (err) {
            console.log(err)
        }
    }
)

router.post(
    '/delete',
    [],
    async (req, res) => {
        try {
            await circles.removeCircle(req.body)
            const coordinates = await circles.getAll()
            res.json(coordinates)
        } catch (err) {
            console.log(err)
        }
    }
)

router.post(
    '/edit',
    [],
    async (req, res) => {
        try {
            await circles.editCircle(req.body)
            const coordinates = await circles.getAll()
            res.json(coordinates)
        } catch (err) {
            console.log(err)
        }
    }
)

router.get(
    '/',
    async (req, res) => {
        const coordinates = await circles.getAll()
        res.json(coordinates)
    }
)

module.exports = router