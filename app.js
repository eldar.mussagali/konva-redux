const express = require('express')
const config = require('config')

const app = express()

const port = config.get('port') || 5000

app.use(express.json())

app.use('/api/rect', require('./routes/rect-routes'))
app.use('/api/circle', require('./routes/circle-routes'))

async function start() {
    try {
        app.listen(port, () => {
            console.log(`App is running on port ${port}...`);
        })
    }
    catch (e) {
        console.log('Error occurred while starting server')
        process.exit(1)
    }
}

start()