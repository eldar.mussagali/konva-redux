import * as actionTypes from '../actions/types'

const initialState = {
    rectangleCoordinates: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_RECTANGLES:
            return {
                rectangleCoordinates: action.payload
            }
        case actionTypes.ADD_RECTANGLE:
            return {
                rectangleCoordinates: action.payload
            }
        case actionTypes.DELETE_RECTANGLE:
            return {
                rectangleCoordinates: action.payload
            }
        case actionTypes.EDIT_RECTANGLE:
            return {
                rectangleCoordinates: action.payload
            }
        default:
            return state
    }
}

export default reducer