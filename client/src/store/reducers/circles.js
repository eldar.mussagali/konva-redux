import * as actionTypes from '../actions/types'

const initialState = {
    circleCoordinates: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_CIRCLES:
            return {
                circleCoordinates: action.payload
            }
        case actionTypes.ADD_CIRCLES:
            return {
                circleCoordinates: action.payload
            }
        case actionTypes.DELETE_CIRCLE:
            return {
                circleCoordinates: action.payload
            }
        case actionTypes.EDIT_CIRCLE:
            return {
                circleCoordinates: action.payload
            }
        default:
            return state
    }
}

export default reducer