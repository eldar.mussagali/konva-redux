import * as actionTypes from './types'

export const getRectangles = () => dispatch => {
    fetch('/api/rect')
        .then(res => res.json())
        .then(rects => (
            dispatch({
                type: actionTypes.GET_RECTANGLES,
                payload: rects
            })
        ))
}

export const addRectangle = rectangle => dispatch => {
    fetch('/api/rect/add', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(rectangle)
    })
        .then(res => res.json())
        .then(rects => (
            dispatch({
                type: actionTypes.ADD_RECTANGLE,
                payload: rects
            })
        ))
}

export const deleteRectangle = rectangle => dispatch => {
    fetch('/api/rect/delete', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(rectangle)
    })
        .then(res => res.json())
        .then(rects => 
            dispatch({
                type: actionTypes.DELETE_RECTANGLE,
                payload: rects
            })
        )
}

export const editRectangle = rectangle => dispatch => {
    fetch('/api/rect/edit', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(rectangle)
    })
        .then(res => res.json())
        .then(rects => 
            dispatch({
                type: actionTypes.EDIT_RECTANGLE,
                payload: rects
            })
        )
}