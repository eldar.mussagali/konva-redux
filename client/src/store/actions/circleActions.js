import * as actionTypes from './types'

export const getCircles = () => dispatch => {
    fetch('/api/circle')
        .then(res => res.json())
        .then(circles => (
            dispatch({
                type: actionTypes.GET_CIRCLES,
                payload: circles
            })
        ))
}

export const addCircle = circle => dispatch => {
    fetch('/api/circle/add', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(circle)
    })
        .then(res => res.json())
        .then(circles => (
            dispatch({
                type: actionTypes.ADD_CIRCLES,
                payload: circles
            })
        ))
}

export const deleteCircle = id => dispatch => {
    fetch('/api/circle/delete', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(id)
    })
        .then(res => res.json())
        .then(circles => 
            dispatch({
                type: actionTypes.DELETE_CIRCLE,
                payload: circles
            })
        )
}

export const editCircle = circle => dispatch => {
    fetch('/api/circle/edit', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(circle)
    })
        .then(res => res.json())
        .then(circles => 
            dispatch({
                type: actionTypes.EDIT_CIRCLE,
                payload: circles
            })
        )
}