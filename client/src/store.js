import { combineReducers, createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'
import rectReducer from './store/reducers/rectangles'
import circleReducer from './store/reducers/circles'

const rootReducer = combineReducers({
  rect: rectReducer,
  circle: circleReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)))