import React, { Component } from 'react'
import { Layer, Circle } from 'react-konva'
import PropTypes from 'prop-types'
import { getCircles, deleteCircle, editCircle } from '../store/actions/circleActions'
import { connect } from 'react-redux'

class Circles extends Component {

    componentDidMount() {
        this.props.getCircles()
    }

    handleDragEnd(e) {
        const id = e.target.id()
        const x = e.target.x()
        const y = e.target.y()
        
        const circle = { id, x, y }

        if ((x > 1350 && x < 1470) && (y >= -50 && y < 50)) {
            this.props.deleteCircle(circle)
        }
        else {
            this.props.editCircle(circle)
        }
    }

    render() {
        return (
            <Layer>
                {this.props.circleCoordinates.map(coordinate => (
                    <Circle
                        key={coordinate.id}
                        id={coordinate.id}
                        x={coordinate.x}
                        y={coordinate.y}
                        radius={35}
                        fill="blue"
                        draggable
                        onDragEnd={e => this.handleDragEnd(e)}
                    />
                ))}
            </Layer>
        )
    }
}

Circles.propTypes = {
    getCircles: PropTypes.func.isRequired,
    deleteCircle: PropTypes.func.isRequired,
    editCircle: PropTypes.func.isRequired,
    circleCoordinates: PropTypes.array.isRequired
}

const mapStateToProps = state => {
    return {
        circleCoordinates: state.circle.circleCoordinates
    }
}

export default connect(mapStateToProps, { getCircles, deleteCircle, editCircle })(Circles)
