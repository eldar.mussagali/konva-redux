import React, { Component } from 'react'
import { addRectangle } from '../store/actions/rectActions'
import { addCircle } from '../store/actions/circleActions'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Layer, Label, Tag, Text } from 'react-konva'

class KonvaButton extends Component {
    render() {
        return (
            <Label
                x={this.props.x}
                y={this.props.y}
                onClick={this.props.click}
            >
                <Tag
                    fill="#7542f5"
                    lineJoin="round"
                >
                </Tag>

                <Text
                    text={this.props.text}
                    fontFamily="Calibri"
                    fontSize={18}
                    padding={5}
                    fill="white"
                >
                </Text>
            </Label>
        )
    }
}

class Buttons extends Component {
    constructor(props) {
        super(props)

        this.handleCircleClick = this.handleCircleClick.bind(this)
        this.handleRectClick = this.handleRectClick.bind(this)
    }

    handleCircleClick() {
        const newCircle = {
            x: Math.floor(Math.random() * 1400),
            y: Math.floor(Math.random() * 680)
        }

        this.props.addCircle(newCircle)
    }

    handleRectClick() {
        const newRect = {
            x: Math.floor(Math.random() * 1400),
            y: Math.floor(Math.random() * 680)
        }

        this.props.addRectangle(newRect)
    }
    render() {
        return (
            <Layer>
                <KonvaButton x={20} y={20} text="Add Rectangle" click={this.handleRectClick} />
                <KonvaButton x={150} y={20} text="Add Circle" click={this.handleCircleClick} />
                <KonvaButton x={1420} y={20} text="Delete" />
            </Layer>
        )
    }
}

Buttons.propTypes = {
    addCircle: PropTypes.func.isRequired,
    addRectangle: PropTypes.func.isRequired,
    rectCoordinates: PropTypes.array.isRequired,
    circleCoordinates: PropTypes.array.isRequired
}

const mapStateToProps = state => {
    return {
        rectCoordinates: state.rect.rectangleCoordinates,
        circleCoordinates: state.circle.circleCoordinates
    }
}

export default connect(mapStateToProps, { addCircle, addRectangle })(Buttons)
