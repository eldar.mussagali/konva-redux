import React, { Component } from 'react'
import { Layer, Rect } from 'react-konva'
import { connect } from 'react-redux'
import { getRectangles, deleteRectangle, editRectangle } from '../store/actions/rectActions'
import PropTypes from 'prop-types'

class Rectangles extends Component {
    componentDidMount() {
        this.props.getRectangles()
    }

    handleDragEnd(e) {
        const id = e.target.id()
        const x = e.target.x()
        const y = e.target.y()

        const rectangle = { id, x, y }

        if ((x > 1350 && x < 1480) && (y >= -50 && y < 50)) {
            console.log(id, x, y);
            this.props.deleteRectangle(rectangle)
        }
        else {
            
            this.props.editRectangle(rectangle)
        }
    }

    render() {
        return (
            <Layer>
                {this.props.rectCoordinates.map(coordinate => (
                    <Rect
                        key={coordinate.id}
                        x={coordinate.x}
                        y={coordinate.y}
                        id={coordinate.id}
                        fill="red"
                        width={70}
                        height={70}
                        draggable
                        onDragEnd={e => this.handleDragEnd(e)}
                    />
                ))}
            </Layer>
        )
    }
}

Rectangles.propTypes = {
    getRectangles: PropTypes.func.isRequired,
    deleteRectangle: PropTypes.func.isRequired,
    editRectangle: PropTypes.func.isRequired,
    rectCoordinates: PropTypes.array.isRequired
}

const mapStateToProps = state => {
    return {
        rectCoordinates: state.rect.rectangleCoordinates
    }
}

export default connect(mapStateToProps, { getRectangles, deleteRectangle, editRectangle })(Rectangles)
