import store from './store'
import { Provider } from 'react-redux'
import './App.css'

import { Stage } from 'react-konva'
import Rectangles from './components/Rectangles'
import Circles from './components/Circles'
import Buttons from './components/Buttons'

function App() {
  return (
    <div className="app">
      <Stage width={window.innerWidth} height={window.innerHeight}>
        <Provider store={store}>
          <Buttons />
          <Rectangles />
          <Circles />
        </Provider>
      </Stage>
    </div>
  );
}

export default App
