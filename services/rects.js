const db = require('./db')
const config = require('config')

async function getAll() {
    const rows = await db.query(`SELECT * FROM RECT`, [])
    return rows
}

async function editRect(req) {
    try {
        const { id, x, y } = req
        await db.query('UPDATE RECT SET x = $2, y = $3 WHERE id=$1', [id, x, y])
    } catch (err) {
        console.log('Error in editRect')
    }
}

async function removeRect(req) {
    try {
        const { id } = req
        await db.query('DELETE FROM RECT WHERE ID=$1', [id])
    } catch (err) {
        console.log('Error in removeRect')
    }
}

async function addRect(req) {
    try {
        const { x, y } = req
        await db.query('INSERT INTO RECT(x, y) VALUES ($1, $2)', [x, y])
    } catch (err) {
        console.log('Error in addRect');
    }
}

module.exports = { getAll, removeRect, addRect, editRect }