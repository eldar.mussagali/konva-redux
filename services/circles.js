const db = require('./db')
const config = require('config')

async function getAll() {
    const rows = await db.query(`SELECT * FROM CIRCLE`, [])
    return rows
}

async function editCircle(req) {
    try {
        const { id, x, y } = req
        await db.query('UPDATE CIRCLE SET x = $2, y = $3 WHERE id=$1', [id, x, y])
    } catch (err) {
        console.log('Error in editCircle')
    }
}

async function removeCircle(req) {
    try {
        const { id } = req
        await db.query('DELETE FROM CIRCLE WHERE ID=$1', [id])
    } catch (err) {
        console.log('Error in removeCircle')
    }
}

async function addCircle(req) {
    try {
        const { x, y } = req
        await db.query('INSERT INTO CIRCLE(x, y) VALUES ($1, $2)', [x, y])
    } catch (err) {
        console.log('Error in addCircle');
    }
}

module.exports = { getAll, removeCircle, addCircle, editCircle }